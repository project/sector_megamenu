<?php

namespace Drupal\sector_megamenu\Plugin\Block;


use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContextAwarePluginInterface;
use Drupal\Core\Menu\MenuTreeParameters;
use Drupal\Core\Menu\MenuLinkTreeInterface;
use Drupal\Core\Cache\Cache;

/**
 * Provides an example block.
 *
 * @Block(
 *   id = "sector_megamenu_body",
 *   admin_label = @Translation("Sector Mega-menu › Body"),
 *   category = @Translation("Sector Mega-menu")
 * )
 */
class MegaMenuBody extends BlockBase implements ContainerFactoryPluginInterface, BlockPluginInterface {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;
  protected $menuLinkTree;

  /**
   * Constructs a new ActiveMenusBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, MenuLinkTreeInterface $menu_link_tree) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->menuLinkTree = $menu_link_tree;
  }


  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'menu' => 'sector-main-menu',
      'maxDepth' => 3,
      'menu_heading_linktext' => 'Learn more'
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('menu.link_tree')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $parameters = new MenuTreeParameters();
    $parameters->maxDepth = $this->configuration['maxDepth'];
    $tree = $this->menuLinkTree->load($this->configuration['menu'], $parameters);
    $manipulators = array(
      // Add more manipulators if needed.
      array('callable' => 'menu.default_tree_manipulators:checkAccess'),
      array('callable' => 'menu.default_tree_manipulators:generateIndexAndSort'),
    );

    $tree = $this->menuLinkTree->transform($tree, $manipulators);

    $menu_output = $this->menuLinkTree->build($tree);

    $menu_output['#heading_link_text'] = $this->configuration['menu_heading_linktext'];
    $menu_output['#theme'] = 'menu__sector_megamenu_body';

    return $menu_output;
  }


  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $menu_options = [];
    $menus = $this->entityTypeManager->getStorage('menu')->loadMultiple();

    foreach($menus as $menuId => $menu) {
      $menu_options[$menuId] = $menu->label();
    }

    $form['maxDepth'] = [
      '#type' => 'number',
      '#title' => t('Maximum depth'),
      '#description' => t('How many menu levels deep do you want your mega menu to render?'),
      '#required' => TRUE, // Optional: Set to TRUE if the field is required.
      '#default_value' => (int)$this->configuration['maxDepth'], // Set a default value if needed.
      '#attributes' => [ 'size' => 10, 'max' => 4, 'min' => 1],
    ];

    $form['menu'] = [
      '#type' => 'select',
      '#title' => t('Which menu should this mega menu work with?'),
      '#options' => $menu_options,
      '#default_value' => $this->configuration['menu'], // Set a default value if needed.
    ];

    $form['menu_heading_linktext'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Heading link text'),
      '#required' => TRUE, // Optional: Set to TRUE if the field is required.
      '#default_value' => (string)$this->configuration['menu_heading_linktext']
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['maxDepth'] = (int) $form_state->getValue('maxDepth');
    $this->configuration['menu'] = $form_state->getValue('menu');
    $this->configuration['menu_heading_linktext'] = $form_state->getValue('menu_heading_linktext');
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    // @see https://www.drupal.org/developing/api/8/cache/contexts.
    // If you depends on \Drupal::routeMatch().
    // You must set context of this block with 'route' context tag.
    // Every new route this block will rebuild.
    return Cache::mergeContexts(parent::getCacheContexts(), ['url.path']);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags(): array {
    return Cache::mergeTags(parent::getCacheTags(), ['sector_megamenu_body']);
  }

}