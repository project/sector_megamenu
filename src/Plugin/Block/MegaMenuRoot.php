<?php

namespace Drupal\sector_megamenu\Plugin\Block;


use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContextAwarePluginInterface;
use Drupal\Core\Menu\MenuTreeParameters;
use Drupal\Core\Menu\MenuLinkTreeInterface;
use Drupal\Core\Cache\Cache;

/**
 * Provides an example block.
 *
 * @Block(
 *   id = "sector_megamenu_root",
 *   admin_label = @Translation("Sector Mega-menu › Root"),
 *   category = @Translation("Sector Mega-menu")
 * )
 */
class MegaMenuRoot extends BlockBase implements ContainerFactoryPluginInterface, BlockPluginInterface {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;
  protected $menuLinkTree;

  /**
   * Constructs a new ActiveMenusBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, MenuLinkTreeInterface $menu_link_tree) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->menuLinkTree = $menu_link_tree;
  }


  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'megamenu_type' => 'pushed',
      'menu' => 'sector-main-menu',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('menu.link_tree')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $menu_name = $this->configuration['menu'];
    $parameters = $this->menuLinkTree->getCurrentRouteMenuTreeParameters($menu_name);
    $parameters->setMinDepth(1);
    $parameters->setMaxDepth(2);
    $parameters->expandedParents = [];

    $expand_all_items = TRUE;

    $tree = $this->menuLinkTree->load($menu_name, $parameters);

    $manipulators = [
      ['callable' => 'menu.default_tree_manipulators:checkAccess'],
      ['callable' => 'menu.default_tree_manipulators:generateIndexAndSort'],
    ];

    $tree = $this->menuLinkTree->transform($tree, $manipulators);
    $build = $this->menuLinkTree->build($tree);

    $build['#theme'] = 'menu__sector_megamenu_root';
    $build['#cache']['contexts'][] = 'route.menu_active_trails:' . $menu_name;
    return $build;
  }


  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $menu_options = [];
    $menus = $this->entityTypeManager->getStorage('menu')->loadMultiple();

    foreach($menus as $menuId => $menu) {
      $menu_options[$menuId] = $menu->label();
    }

    $form['megamenu_type'] = [
      '#type' => 'radios',
      '#title' => t('Select an option'),
      '#options' => [
        'overlay' => 'Overlaid (coming soon)',
        'pushed' => 'Pushed',
      ],
      '#attributes' => [
        'disabled' => TRUE,
      ],
      '#default_value' => $this->configuration['megamenu_type'], // Set a default value if needed.
    ];

    $form['menu'] = [
      '#type' => 'select',
      '#title' => t('Select a menu'),
      '#options' => $menu_options,
      '#default_value' => $this->configuration['menu'], // Set a default value if needed.
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['megamenu_type'] = $form_state->getValue('megamenu_type');
    $this->configuration['menu'] = $form_state->getValue('menu');
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    // @see https://www.drupal.org/developing/api/8/cache/contexts.
    // If you depends on \Drupal::routeMatch().
    // You must set context of this block with 'route' context tag.
    // Every new route this block will rebuild.
    return Cache::mergeContexts(parent::getCacheContexts(), ['url.path']);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags(): array {
    return Cache::mergeTags(parent::getCacheTags(), ['sector_megamenu_root']);
  }
}