## Sector MegaMenu add-on

The [Sector Mega Menu module](https://www.drupal.org/project/sector_megamenu) — is built on the Drupal core Menu module and the Drupal contribution module: [Menu item content fields](https://www.drupal.org/project/menu_item_fields). This module has a hard dependency on the _Sector › Main menu_ and its _menu block_ from the [Sector Distribution](https://www.drupal.org/project/sector).

## Why a mega menu?

Users often get lost in sites with a large amount of pages in a hierarchical menu.

The mega menu can help with that, as the user can easy grasp what is in a section, see all links to pages in deeper levels, without the need to ‘click through’ to open further menus.

You may also want to show other entities: content, media, view in the mega menu.

## How it works
When installing the module, two new blocks will be added with template suggestions of:

1. sector_main_menu_megamenu
2. sector_main_menu_megamenu_placeholder

An accompanying Javascript function is looking for its source at `.region--header.header #nav-main-navigation--megamenu > ul#main-navigation--megamenu`.
On click of that menu's level 1 items, it stops the default event and opens the Mega menu placeholder block where that is placed, and clones that clicked menu's sub navigation into that placeholder. This include the clicked menu link itself so the user can still navigate to top level items.
